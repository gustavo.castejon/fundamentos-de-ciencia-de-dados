# Fundamentos de ciência de dados 

## Comandos para versionamento (git)

Digitar os comandos abaixo na raíz do projeto para versionar o código e subí-lo no gitlab

```
git add .
git commit -m "insira a mensagem aqui"
git pull origin main
git push origin main
```

## Criação do ambiente virtual (conda)

```
conda activate env_ gustavo
conda env update
```

## Comandos básicos para o terminal Linux

```
ls
cd
mkdir
history
cat
```

```
Tarefa para 05/05
Acrescentar 10 países, colocar idiomas, população e continente 
"1 - Informações sobre Brasil: português, 200 milhões, América"
```
```
Tarefa para 12/05
- input com nome de um país, caso for satisfeito será printado as características delimitadas. Caso não for satisfeito, apontar com o else que o país não consta.
- utilizar o return (passar lista de países já existentes e acrescentar um novo país à lista).
- Trazer o tema de pesquisa + indicação de 3 fontes de dados que são importantes para as pesquisas pessoais (ex. sites)
```
```
Tarefa para 19/05
- Passar informações dos países utilizando o return, o break e o continue
- Utilizar informações de idioma, países, continentes e população
```
```
Tarefa 26/5
- Escolher os 3 sites e realizar as mesmas funções da reuniao do dia 19/5
- Entender o uso do request e como o bs percorre as paginas extraindo informações
```

```
Entrar nos links e extrair os parágrafos
Consultar a info do link para extra
```