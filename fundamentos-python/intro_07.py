#def if_elif_else ():
    #pass
    #país=input("Digite o nome de um país: ")
    #países= ["Rússia", "Síria", "Líbano", "Filipinas", "Noruega"]
    #idiomas= ["russo", "sírio", "libanês", "filipino", "norueguês"]
    #populações= ["144 mi", "17 mi", "7 mi", "109 mi", "5 mi"]
    #continentes= ["Eurásia", "Oriente Médio", "Oriente Médio", "Europa", "Europa"]
    if (país==países[0]): 
            print (f"o idioma é {idiomas[0]}, a população é {populações[0]} e o continente é {continentes[0]} ")
    elif (país==países[1]): 
            print (f"o idioma é {idiomas[1]}, a população é {populações[1]} e o continente é {continentes[1]} ")
    elif (país==países[2]): 
            print (f"o idioma é {idiomas[2]}, a população é {populações[2]} e o continente é {continentes[2]} ")
    elif (país==países[3]): 
            print (f"o idioma é {idiomas[3]}, a população é {populações[3]} e o continente é {continentes[3]} ")
    elif (país==países[4]): 
            print (f"o idioma é {idiomas[4]}, a população é {populações[4]} e o continente é {continentes[4]} ")
    elif (país==países[5]): 
            print (f"o idioma é {idiomas[5]}, a população é {populações[5]} e o continente é {continentes[5]} ")
    else:
        print(f"O país {país} não consta na lista.")
    
    
#def instrução_return ():
    pass 
    países= ["Rússia", "Síria", "Líbano", "Filipinas", "Noruega"]
    novo_país= input("Digite um país: ")
    for país in países:
        if (país=="Rússia") or (país=="Síria") or (país=="Líbano") or (país=="Filipinas") or (país=="Noruega"):
            print(f"{país} já consta na lista.")
        if país!=novo_país:
            nomes.append(novo_país)
    #print(nomes)
    países= list(set(países))
    #print(nomes)
    return países

def instrução_return ():
    país=input("Digite o nome de um país: ")
    países= ["Rússia", "Síria", "Líbano", "Filipinas", "Noruega"]
    idiomas= ["russo", "sírio", "libanês", "filipino", "norueguês"]
    populações= ["144 mi", "17 mi", "7 mi", "109 mi", "5 mi"]
    continentes= ["Eurásia", "Oriente Médio", "Oriente Médio", "Europa", "Europa"]
    if país in países:
        print (f"{país}, {idiomas}, {populações}, {continentes}")
    elif not país in países:
        países.append(país)
        print(país)
    #print(países,len(países))
    return países
        
def instrução_break ():
    '''
    Utilizado apenas dentro do loop_for
    Interrompe o loop_for e vai para o próximo bloco do código
    '''
    #países=instrução_return()[0]
    #for país in países:
     #   print(país)
    países=instrução_return()
    for país in países:
        if país=="Síria":
            print("Este país está na lista.")
            break
        if país=="Noruega":
            print("Este país está na lista.")
            break
        if país=="Congo":
            print("Este país não está na lista.")
    print("Saiu do loop_for.")

def instrução_continue ():
    países=instrução_return()
    for país in países:
        if país=="Rússia":
            continue
            print("Este país está na lista.")
        if país=="Brasil":
            print("Este país não está na lista.")
    print("Saiu do loop_for.")


def instrução_try_except ():
    pass






def main ():
    #if_elif_else ()
    instrução_break ()
    instrução_return()
    instrução_continue ()

if __name__=="__main__":
    main()