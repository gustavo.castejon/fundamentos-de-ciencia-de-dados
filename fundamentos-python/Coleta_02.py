import requests
from bs4 import BeautifulSoup
import urllib.parse

def acessar_pagina(url):
    pagina=requests.get(url)
    bs=BeautifulSoup (pagina.text, "html.parser")
    return bs

def paginas():
    paginas = []
    contador = 1 #colocar contador
    while contador >=0:
        if contador != 0:
            url = "https://dataprivacy.com.br/category/artigos/"
            url = url + str(contador)
            contador = contador - 1
            paginas.append(url)
        elif contador == 0:
            url = "https://dataprivacy.com.br/category/artigos/" 
            contador = contador -1
            paginas.append(url)
    print (paginas)
    return paginas


def coleta_data_privacy_brasil():
    url = "https://dataprivacy.com.br/category/artigos/"
    bs = acessar_pagina(url)
    lista_de_tags_artigos = bs.find_all("article") # find_all retorna uma lista
    #print(lista_de_tags_artigos)
    for tag_article in lista_de_tags_artigos:
        titulo = tag_article.a.find("h2", attrs={"class":"articles-item__title"}) #tag_article.a.h2.text
        link = tag_article.a["href"] #href entre aspas pois é um meta dado e não uma tag
        data= bs.find("div",attrs={"class":"articles-item__meta"}).find_all("span")[0].text
        tag= bs.find ("div",attrs={"class":"articles-item__meta"}).text
        print(titulo.text)
        print(link)
        print(data)
        print(tag)
        #page_noticia= acessar_pagina(link)
        #tag_paragrafos= page_noticia.find("div",attrs={"class":"single-content reveal-item reveal-item--is-visible"}).find_all("p") 
        #print(tag_paragrafos)
        #paragrafos=[]
        #for paragrafo in tag_paragrafos:
        #paragrafos.append(paragrafo.text)
        #print(paragrafos)
        #print("#########")

def coleta_bcp_policy_brief ():
    for index, pagina in enumerate(paginas(), start=1):
        print(index, pagina)
        bs=acessar_pagina(pagina)
        lista_de_tags_artigos=bs.find_all ("article") #find all retorna uma lista
        for tag_article in lista_de_tags_artigos:
            titulo=tag_article.a["title"]
            link=tag_article.a["href"]
            print (titulo)
            print (link)
            print ("#########")


def main ():
    coleta_data_privacy_brasil()

if __name__ == "__main__":
    main ()