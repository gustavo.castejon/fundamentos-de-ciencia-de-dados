def loop_for():
    países= ["Rússia", "Síria", "Líbano", "Filipinas", "Noruega", "Dinamarca", "Nicarágua", "Cuba", "Austrália", "República Democrática do Congo"]
    idiomas= ["russo", "sírio", "libanês", "filipino", "norueguês", "dinamarquês", "castelhano", "espanhol", "inglês", "francês"]
    populações= ["144 mi", "17 mi", "7 mi", "109 mi", "5 mi", "6 mi", "6 mi", "11 mi", "26 mi", "89 mi" ]
    continentes= ["Eurásia", "Oriente Médio", "Oriente Médio", "Europa", "Europa", "América Central", "América do Norte","Oceania", "África"]
    
    for país, idioma, população, continente in zip(países, idiomas, populações, continentes):
        print(f"O nome do país, seu idioma, população e continente são: {país},{idioma},{população},{continente}")
        print(f"O nome do país: {país}, tem como idioma: {idioma}, população de: {população} e continente:{continente}")
       
def range_enumerate ():
    países= ["Rússia", "Síria", "Líbano", "Filipinas", "Noruega", "Dinamarca", "Nicarágua", "Cuba", "Austrália", "República Democrática do Congo"]
    idiomas= ["russo", "sírio", "libanês", "filipino", "norueguês", "dinamarquês", "castelhano", "espanhol", "inglês", "francês"]
    populações= ["144 mi", "17 mi", "7 mi", "109 mi", "5 mi", "6 mi", "6 mi", "11 mi", "26 mi", "89 mi" ]
    continentes= ["Eurásia", "Oriente Médio", "Oriente Médio", "Europa", "Europa", "América Central", "América do Norte","Oceania", "África"]
    for index,(país, idioma, população, continente) in enumerate(zip(países, idiomas, populações, continentes),start=1):
        print (index,país, idioma, população, continente)

def main():
    #loop_for()
    range_enumerate()

if __name__=="__main__":
    main()
   

 #print(f"O nome do país é: {país}")
    
    #for idioma in idiomas:
        #print(f"O idioma do país é: {idioma}")
    
    #for população in populações:
        #print(f"A população do país é: {população}")
    
    #for continente in continentes:
        #print(f"O continente do país é: {continente}")