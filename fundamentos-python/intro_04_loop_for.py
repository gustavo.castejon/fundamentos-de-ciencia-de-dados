def loop_for():
    países=["Brasil","Argentina","África do Sul","Portugal"]  
    idiomas=["português", "espanhol", "inglês", "português"]
    #print(países, type (países))
    for país, idioma in zip (países, idiomas):
        #print(país)
        print(f"O nome do país é: {país}")
        print(f"O idioma do país é: {idioma} ")
    teste=[país.upper() for país in idiomas]
    print (teste)
    novos_países=["Alemanha", "Egito", "Japão"]
    for país in novos_países:
        caixa_alta=país.upper()
        países.append(caixa_alta)
    print (países)
    
def range_enumerate ():
    novos_países=["Alemanha", "Egito", "Japão"]
    for i in range(20,50,5):
        print (i)
    for index,país in enumerate(novos_países, start=1):
        print (index,país)

def loop_while ():
    print ("loop_while")
    a=10
    while a>0:
        print(a)
        a-=1
        #a=a-1 ou a-=1 são equivalentes

def exemplo_loop_while ():
    contador=240
    while contador>0:
        url="https://www.gov.br/mre/pt-br/canais_atendimento/imprensa/notas-a-imprensa?b_start:int="
        url= url+str(contador)
        contador= contador-30
        print (url)

def main ():
   #loop_for()
   #range_enumerate() 
   loop_while()
    #exemplo_loop_while()
    
    #print ("teste")

if __name__ == "__main__":
    main ()
