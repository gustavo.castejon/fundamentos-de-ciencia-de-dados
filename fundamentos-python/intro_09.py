def instrução_return ():
    país=input("Digite o nome de um país: ")
    países= ["Rússia", "Síria", "Líbano", "Filipinas", "Noruega"]
    idiomas= ["russo", "sírio", "libanês", "filipino", "norueguês"]
    populações= ["144 mi", "17 mi", "7 mi", "109 mi", "5 mi"]
    continentes= ["Eurásia", "Oriente Médio", "Oriente Médio", "Europa", "Europa"]
    if país =="Rússia":
        print (f"{país}, {idiomas[0]}, {populações[0]}, {continentes[0]}")
    elif país =="Síria":
        print (f"{país}, {idiomas[1]}, {populações[1]}, {continentes[1]}")
    elif país =="Líbano":
        print (f"{país}, {idiomas[2]}, {populações[2]}, {continentes[2]}")
    elif país =="Filipinas":
        print (f"{país}, {idiomas[3]}, {populações[3]}, {continentes[3]}")
    elif país =="Noruega":
        print (f"{país}, {idiomas[4]}, {populações[4]}, {continentes[4]}")

    elif not país in países:
        países.append(país)
        print(países)
    return países
        
def instrução_break ():
    países=instrução_return()
    for país in países:
        if país=="Síria":
            print("Este país está na lista.")
            break
        if país=="Noruega":
            print("Este país está na lista.")
            break
        if país=="Congo":
            print("Este país não está na lista.")
    print("Saiu do loop_for.")

def instrução_continue ():
    países=instrução_return()
    for país in países:
        if país=="Rússia":
            continue
            print("Este país está na lista.")
        if país=="Brasil":
            print("Este país não está na lista.")
    print("Saiu do loop_for.")


def instrução_try_except ():
    pass

def main ():
    #if_elif_else ()
    #instrução_break ()
    instrução_return()
    #instrução_continue ()

if __name__=="__main__":
    main()