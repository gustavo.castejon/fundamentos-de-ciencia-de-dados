from tinydb import TinyDB,Query
import os

def inserir_db(titulo="NA",link="NA",tag="NA",data="NA",autoria="NA",idiomas="NA",site="NA", paragrafos="NA"):
    db=TinyDB(f"/home/lantri_gustavooliveira/codigo/fundamentos-de-ciencia-de-dados/fundamentos-python/{site}.json", indent=4, ensure_ascii=False)
    buscar=Query()
    verificar_db=db.contains((buscar.titulo==titulo)&(buscar.data==data))
    if not verificar_db:
        db.insert({
            "titulo":titulo,
            "link":link,
            "tag": tag,
            "data": data,
            "autoria": autoria,
            "idiomas": idiomas
         })
    else:
        print("já está na base")
    if paragrafos != "NA":
        print("inserindo paragrafos")
        atualizar_db(site,paragrafos)

def atualizar_db(site="NA",paragrafos="NA",idiomas="NA",titulo="NA"):
    db=TinyDB(f"/home/lantri_gustavooliveira/codigo/fundamentos-de-ciencia-de-dados/fundamentos-python/{site}.json", indent=4, ensure_ascii=False)
    buscar=Query()
    db.upsert({
        "idiomas":idiomas
    },buscar.titulo==titulo)

def main ():
   #inserir_db ()
   atualizar_db(site="cgi")
 
   


if __name__ == "__main__":
    main ()