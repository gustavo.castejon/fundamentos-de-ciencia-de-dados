import requests
from bs4 import BeautifulSoup

def acessar_pagina(url):
    pagina=requests.get(url)
    bs=BeautifulSoup (pagina.text, "html.parser")
    return bs

def paginas():
    paginas = []
    contador = 8
    while contador >=0:
        if contador != 0:
            url = "https://opiceblum.com.br/category/artigos/page/"
            url = url + str(contador)
            contador = contador - 1
            paginas.append(url)
        elif contador == 0:
            url = "https://opiceblum.com.br/category/artigos/" 
            contador = contador -1
            paginas.append(url)
    #print (paginas)
    return paginas

def extrair_info ():
    '''Extração das publicações do Opice Blum'''
    for index, pagina in enumerate(paginas(), start=1):
        print(index, pagina)
        bs=acessar_pagina(pagina)    
        lista_de_tags_artigos = bs.find_all("article")
    #print (lista_de_tags_artigos)
        for tag_article in lista_de_tags_artigos:
            titulo = tag_article.a.text
            link = tag_article.a["href"]
            autor = bs.find("div",attrs={"class":"elementor-post__excerpt"}).find_all("p")[0].text
            data= bs.find("div", attrs={"class":"elementor-post__meta-data"}).find_all("span")[0].text
            #metadados= bs.find("div",attrs={"class":"publicacao-info caps"})
            #tipo= metadados.find_all("span")[1].text
            #publicado= metadados.find_all("span")[5].text
            #idiomas= metadados.find_all("span")[7].text
            print(titulo)
            print(link) 
            print(autor) 
            print(data)        
            print("#########")




def main ():
   extrair_info ()
   


if __name__ == "__main__":
    main ()
