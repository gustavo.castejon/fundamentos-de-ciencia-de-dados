import requests
from bs4 import BeautifulSoup
from banco_json import inserir_db, atualizar_db
from tinydb import TinyDB,Query

def acessar_pagina(url):
    pagina=requests.get(url)
    bs=BeautifulSoup (pagina.text, "html.parser")
    return bs

def paginas():
    paginas = []
    contador = 27
    while contador >=1:
        if contador != 1:
            url = "https://www.cgi.br/publicacoes/indice/page:"
            url = url + str(contador)
            contador = contador - 1
            paginas.append(url)
        elif contador == 1:
            url = "https://www.cgi.br/publicacoes/indice/" 
            contador = contador -1
            paginas.append(url)
    print (paginas)
    return paginas

def extrair_info ():
    '''Extração das publicações do CGIBr'''
    for index, pagina in enumerate(paginas(), start=1):
        print(index, pagina)
        bs=acessar_pagina(pagina)    
        lista_de_tags_artigos = bs.find_all("section")
    #print (lista_de_tags_artigos)
        for tag_article in lista_de_tags_artigos:
            titulo = tag_article.a.text
            link_relativo = tag_article.a["href"]
            dominio= "https://www.cgi.br"
            link= dominio + link_relativo
            metadados= bs.find("div",attrs={"class":"publicacao-info caps"})
            tag= metadados.find_all("span")[1].text
            data_tmp= metadados.find_all("span")[3].text
            autoria= metadados.find_all("span")[5].text.strip()
            idiomas= metadados.find_all("span")[7].text.strip()
            link_info= coletar_paragrafos()
            bs1=acessar_pagina(link_info)
            lista_link_pdf= tag_article.find_all("div", attrs={"class":"col-lg-2 col-md-3 col-sm-3"})
            print(link)
            link_pdf=[]
            for pdf in lista_link_pdf:
               link_pdf.append(pdf["href"])
            #if not link_pdf: 
             #   print("entrou na lista de paragrafos")
              #  bs=acessar_pagina(link)
               # lista_paragrafos=bs.find("body")
                #paragrafos=lista_paragrafos

           # /html/body/div[1]/div/div[1]/div[3]/div/main/section/div[3]/div[1]/a
            print (link_pdf)
           # print(paragrafos)
          #  print(lista_link_pdf)
            #paragrafos = encontrar paragrafos no inspecionar
            print(tag)
            print(titulo)
            print(autoria)
            print(idiomas)
            data=ajustar_data(data_tmp)
            print(data)
            print("#########")
            site="cgi"
            inserir_db(titulo=titulo,link=link,tag=tag,data=data,autoria=autoria,idiomas=idiomas,site=site)

def coletar_paragrafos(site="cgi"):
    db=TinyDB(f"/home/lantri_gustavooliveira/codigo/fundamentos-de-ciencia-de-dados/fundamentos-python/{site}.json", indent=4, ensure_ascii=False)
    buscar=Query()
    for info in iter(db):
       link=info["link"]
       return link


def ajustar_data(data):
    meses={
        "janeiro":"01",
        "fevereiro":"02",
        "março":"03",
        "abril":"04",
        "maio":"05",
        "junho":"06",
        "julho":"07",
        "agosto":"08",
        "setembro":"09",
        "outubro":"10",
        "novembro":"11",
        "dezembro":"12"
        }
    data_lista=data.split()
    #print(data_lista[2])
    mes=meses[data_lista[2]]
    #print(mes)
    data_normalizada=f"{data_lista[0]}/{mes}/{data_lista[-1]}"
    return (data_normalizada)

def ajuste_json(site="cgi"):
    db=TinyDB(f"/home/lantri_gustavooliveira/codigo/fundamentos-de-ciencia-de-dados/fundamentos-python/{site}.json", indent=4, ensure_ascii=False)
    buscar=Query()
    for info in iter(db):
       if info ["idiomas"]=="PortuguÃªs":
            idioma=info["idiomas"]
            titulo=info["titulo"]
            converter=idioma.encode("iso-8859-1").decode("utf-8")
            print(converter)
            atualizar_db(site=site,titulo=titulo,idiomas=converter)
            #criar essa mesma estrutura para o titulo também
            
       
    


def main ():
   #extrair_info ()
   ajuste_json ()
    

if __name__ == "__main__":
    main ()
