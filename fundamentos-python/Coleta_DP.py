import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
from time import sleep
import lxml
from banco_json import inserir_db,atualizar_db

def acessar_pagina (pag):
    html=requests.get(pag)
    bs=BeautifulSoup(html.text,"html.parser")
    return bs

def data_privacy_brasil_carregar_mais():
    '''Responsavel por clicar no botão carregar mais e passar as informacoes carregadas para o BeautifulSoup''' 
    navegador = webdriver.Firefox()
    navegador.get("https://dataprivacy.com.br/publicacoes/")
    #onetrust-accept-btn-handler
    sleep(3)
    cookie=navegador.find_element(By.CSS_SELECTOR,"#onetrust-accept-btn-handler")
    cookie.click()
    sleep(2)
   # body > div.general-container > section > div > div.load-more__wrapper > span 
    carregar_mais=WebDriverWait(navegador,5).until(EC.element_to_be_clickable((By.CSS_SELECTOR,"body > div.general-container > section > div > div.load-more__wrapper > span")))
    for _ in range(12):
        print("carregar_mais")
        carregar_mais.click()
        sleep(2)
    bs=BeautifulSoup(navegador.page_source,"lxml")
    navegador.close()
    return bs
def extrair_infos():
    bs= data_privacy_brasil_carregar_mais()
    lista_de_tags_artigos = bs.find_all("article") 
    for tag_article in lista_de_tags_artigos:
        titulo = tag_article.a.find("h2", attrs={"class":"articles-item__title"}).text 
        link = tag_article.a["href"] 
        data_tmp= tag_article.a.find("div",attrs={"class":"articles-item__meta"}).find("span").text
        data= data_tmp.replace(".","/")
        tag= tag_article.a.find ("div",attrs={"class":"articles-item__meta"}).text[14:]
        pagina=acessar_pagina(link)
        tag_p=pagina.find("section").find_all("p")
        paragrafos=[]
        for paragrafo in tag_p:
            paragrafos.append(paragrafo.text)
        print(titulo)
        print(link)
        print(tag)
        print(data)
        print(paragrafos)
        site="data_privacy"
        #atualizar_db(site,paragrafos)
        inserir_db(titulo=titulo,link=link,tag=tag,data=data,site=site, paragrafos=paragrafos)   

def main ():
   extrair_infos()
 
   


if __name__ == "__main__":
    main ()

 
   


if __name__ == "__main__":
    main ()