import requests
from bs4 import BeautifulSoup

#CGIBrasil
#TargetData
#InterneteSociedade(InternetLab)

def acessar_pagina(url):
    pagina = requests.get(url)
    bs = BeautifulSoup(pagina.text, "html.parser")
    return bs

def coleta_data_privacy_brasil():
    url = "https://dataprivacy.com.br/category/artigos/"
    bs = acessar_pagina(url)
    lista_de_tags_artigos = bs.find_all("article") # find_all retorna uma lista
    #print(lista_de_tags_artigos)
    for tag_article in lista_de_tags_artigos:
        titulo = tag_article.a.find("h2", attrs={"class":"articles-item__title"}) #tag_article.a.h2.text
        link = tag_article.a["href"] #href entre aspas pois é um meta dado e não uma tag
        #titulo2 = tag_article.h4.text
        print(titulo)
        print(link)
        #print(titulo2)
        print("#########")
        

def coleta_CGI_Br ():
    url = "https://www.cgi.br/publicacoes/indice/"
    bs = acessar_pagina(url)
    lista_de_tags_artigos = bs.find_all("article")
    #print (lista_de_tags_artigos)
    for tag_article in lista_de_tags_artigos:
        titulo = tag_article.a.text
        link = tag_article.a["href"]
        print(titulo)
        print(link)
        print("#########")

def coleta_bpc_policy_brief():
    """coletar autores, titulo, resumo e pdf"""
    url = "https://bricspolicycenter.org/publicacoes-pt/bpc-policy-brief/page/2/"
    bs = acessar_pagina(url)
    lista_de_tags_artigos = bs.find_all("article") # find_all retorna uma lista
    for tag_article in lista_de_tags_artigos:
        titulo = tag_article.a["title"]
        link = tag_article.a["href"]
        titulo2 = tag_article.h4.text
        print(titulo)
        print(link)
        print(titulo2)
        print("#########")



def main():
   # coleta_data_privacy_brasil()
    coleta_CGI_Br   ()

if __name__=="__main__":
    main()
    

