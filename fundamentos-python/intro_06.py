def if_elif_else ():
    a,b,c= 10, 20, 30
    nome= input("Digite um nome: ")
    nome1= "Bruna"
    nome2= "Treyce"
    nome3= "Gustavo"
    nome4= "João"
    if nome != nome1: #se
        print (nome1)
    else: #se não
        print ("se não")
    if nome == nome1: #if not - condição falsa
        print (nome1)
    elif nome == nome2:
        print (nome2)
        print("Você entrou no segundo condicional.")
    elif nome == nome3:
        print(nome3)
    elif nome == nome4:
        print(nome4)
    else:
        print(f"O nome {nome} não tem correspondente.")
    
def instrução_return ():
    país=input("Digite o nome de um país: ")
    países= ["Rússia", "Síria", "Líbano", "Filipinas", "Noruega"]
    if país in países:
        #print (país)
        pass
    elif not país in países:
        países.append(país)
        #print(país)
    #print(países,len(países))
    return países
        
def instrução_break ():
    '''
    Utilizado apenas dentro do loop_for
    Interrompe o loop_for e vai para o próximo bloco do código
    '''
    #países=instrução_return()[0]
    #for país in países:
     #   print(país)
    países=instrução_return()
    for país in países:
        if país=="Rússia":
            print("Este país está na lista.")
            break
        if país=="Brasil":
            print("Este país não está na lista.")
    print("Saiu do loop_for.")

def instrução_continue ():
    países=instrução_return()
    for país in países:
        if país=="Rússia":
            continue
            print("Este país está na lista.")
        if país=="Brasil":
            print("Este país não está na lista.")
    print("Saiu do loop_for.")

def instrução_try_except ():
    países=instrução_return()
    try:
        print(idioma)
    except:
        pass






def main ():
    #if_elif_else ()
    #países=instrução_return()
    #print(países[0])
    #print(países[1])
    instrução_break()
    instrução_continue()
    instrução_try_except()

if __name__=="__main__":
    main()